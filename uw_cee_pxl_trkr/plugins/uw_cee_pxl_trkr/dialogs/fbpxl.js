/**
 * @file
 */

(function () {
    // Variable used to track when the form is submitted.
    // i.e to check for double submissions on using enter key in dialog.
    var formSubmitted = false;
    var fbpxlDialog = function (editor) {
        return {
            title : 'FB Tracking Pixel',
            minWidth : 625,
            minHeight : 100,
            contents: [{
                id: 'fbpxl',
                label: 'fbpxl',
                elements:[{
                    type: 'checkbox',
                    id: 'fbpxlInput',
                    label: 'Check box to confirm inclusion of pixel',
                    setup: function (element) {
                    }
                },]
            }],
            onOk: function () {
                // Get form information.
                fbpxlInput = this.getValueOf('fbpxl','fbpxlInput');
                // Validate input. Note that there is probably a CKEditor specific way to do this, but this works.
                errors = '';

                // If form has been submitted before then set it back to not being seeing before.
                // i.e if this is double submission set it back to not being run before.
                if (formSubmitted == true) {
                    formSubmitted = false;
                    return false;
                }
                // Only display errors if there are errors to display and the form has not been run before.
                else if (errors && formSubmitted == false) {
                    alert(errors);
                    formSubmitted = true;
                    return false;
                } else if(fbpxlInput != true){
                    alert('Please check box in order to add pixel.');
                    formSubmitted = false;
                    return false
                } else {
                    // Create the fbpxl element.
                    var ckfbpxlNode = new CKEDITOR.dom.element('ckfbpxl');
                    // Set fake object name
                    CKEDITOR.lang.en.fakeobjects.fbpxl = CKEDITOR.uw_cee_pxl_trkr.ckfbpxl;
                    // Create the fake image for this element and insert into the document (realElement, className, realElementType, isResizable).
                    var newFakeImage = editor.createFakeElement(ckfbpxlNode, 'ckfbpxl', 'ckfbpxl', false);
                    newFakeImage.addClass('fbpxl');
                    if (this.fakeImage) {
                        newFakeImage.replace(this.fakeImage);
                        editor.getSelection().selectElement(newFakeImage);
                    }
                    else {
                        editor.insertElement(newFakeImage);
                    }
                    // Reset title.
                    CKEDITOR.lang.en.fakeobjects.ckfbpxl = CKEDITOR.uw_cee_pxl_trkr.ckfbpxl;
                }
            },
            onShow: function () {
                // Set up to handle existing items.
                this.fakeImage = this.ckfbpxlNode = null;
                var fakeImage;

                // Check if element is right clicked or icon was clicked, if not use global varaible doubleclick_element.
                if (this.getSelectedElement()) {
                    fakeImage = this.getSelectedElement();
                }
                else {
                    fakeImage = doubleclick_element;
                }

                if (fakeImage && fakeImage.data('cke-real-element-type') && fakeImage.data('cke-real-element-type') === 'ckfbpxl') {
                    this.fakeImage = fakeImage;
                    var ckfbpxlNode = editor.restoreRealElement(fakeImage);
                    this.ckfbpxlNode = ckfbpxlNode;
                    this.setupContent(ckfbpxlNode);
                }
            }
        }
    };

    CKEDITOR.dialog.add('fbpxl', function (editor) {
        return fbpxlDialog(editor);
    });
})();
