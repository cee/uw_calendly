/**
 * @file
 * ckeditor pixel tracking plug-in for CEE
 */
var doubleclick_element;

CKEDITOR.plugins.add('uw_cee_pxl_trkr', {
    requires : ['dialog', 'fakeobjects'],
    init: function (editor){
        //Define plugin names.
        var pluginName = 'uw_cee_pxl_trkr';

        //Set up objects to share variables amongst scripts.
        CKEDITOR.uw_cee_pxl_trkr = {};

        //Register button for Calendly
        editor.ui.addButton('fbpxl', {
            label : "Add/Edit FB Tracking Pixel",
            command : 'fbpxl',
            icon : this.path + 'icons/fbpxl.png'
        });

        //Register right-click menu item for uw_cee_uw_cee_pxl_trkr
        editor.addMenuItems({
            calendly : {
                label: "Edit FB Pixel",
                icon : this.path + 'icons/fbpxl.png',
                command : 'fbpxl',
                group : 'image',
                order : 1
            }
        });
        //Make ckcalendly a self-closing tag.
        CKEDITOR.dtd.$empty['ckfbpxl'] = 1;
        //Making sure that the fake element for calendly has a name.
        CKEDITOR.uw_cee_pxl_trkr.ckfbpxl = 'FB pixel widget';
        CKEDITOR.lang.en.fakeobjects.ckfbpxl = CKEDITOR.uw_cee_pxl_trkr.ckfbpxl;
        //Add JS file that defines the dialog box for uw_cee_uw_cee_pxl_trkr
        CKEDITOR.dialog.add('fbpxl',this.path + 'dialogs/fbpxl.js');
        editor.addCommand('fbpxl', new CKEDITOR.dialogCommand('fbpxl'));

        //Open the appropriate dialog box if an element is double-clicked.
        editor.on('doubleclick', function (evt) {
            var element = evt.data.element;

            //Store in global variable.
            doubleclick_element = element;

            if (element.is('img') && element.data('ck-real-element-type') === 'ckfbpxl') {
                evt.data.dialog = 'fbpxl';
            }
        });

        //Adding the appropriate right-click menu item if an element is right-clicked
        if (editor.contextMenu) {
            editor.contextMenu.addListener(function (element,selection) {
                if (element && element.is('img') && element.data('ck-real-element-type') === 'ckfbpxl'){
                    return { fbpxl : CKEDITOR.TRISTATE_OFF };
                }
            });
        }

        //Add CSS to use in-editor to style custom fake elements.
        CKEDITOR.addCss(
            'img.ckfbpxl {' +
            'background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/fbpxl.png') + ');' +
            'background-position: center center;' +
            'background-repeat: no-repeat;' +
            'background-color: #000;' +
            'width: 100%;' +
            'height: 50px;' +
            'margin: 0 0 10px 0;' +
            'margin-left: auto;' +
            'margin-right: auto;' +
            '}'
        );
    },
    afterInit : function (editor) {
        //make fake image display on first load/return from source view
        if (editor.dataProcessor.dataFilter){
            editor.dataProcessor.dataFilter.addRules({
                elements: {
                    ckfbpxl : function (element) {
                        //Reset tittle
                        CKEDITOR.lang.en.fakeobjects.ckfbpxl = CKEDITOR.uw_cee_pxl_trkr.ckfbpxl;
                        //Note that this just accepts whatever attributes are on the element; may want to filter these.
                        return editor.createFakeParserElement(element, 'ckfbpxl', 'ckfbpxl', false);
                    }
                }
            });
        }
    }
});
