![banner](../images/pixel_tracker_banner.jpg )
## Pixel Tracker Module
#### Current supported pixels:
*Facebook Pixel*
#### Use:
This module is intended to add pixel tracking for CEE. A custom CKEditor widget is added which will enable pixel tracking on only specific web pages.
#### Setup:
1. Enable plug-in at admin/config/content/wysiwyg/profile by selecting a profile and enabling the desired plugins
2. Edit filters to enable ckfbpxl element tags.

#### Dialog Box Options:
##### FB Pixel:
>Ensure that the checkmark is checked before clicking on ok.

>A specialized Facebook banner should appear confirming that the tracking pixel has been added to the page

*Credits to Kimly Truong for icons*