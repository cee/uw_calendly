/**
 * @file
 */

(function ($) {

    Drupal.behaviors.uw_cee_pxl_trkr = {
        attach: function (context, settings) {
            // Add Pixel Tracking if ckfbpxl element exists
            if ($('ckfbpxl').length) {
                var fbpxlScript =  "<script>"+
                    "!function(f,b,e,v,n,t,s)\n" +
                    "  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?\n" +
                    "  n.callMethod.apply(n,arguments):n.queue.push(arguments)};\n" +
                    "  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';\n" +
                    "  n.queue=[];t=b.createElement(e);t.async=!0;\n" +
                    "  t.src=v;s=b.getElementsByTagName(e)[0];\n" +
                    "  s.parentNode.insertBefore(t,s)}(window, document,'script',\n" +
                    "  'https://connect.facebook.net/en_US/fbevents.js');\n" +
                    "  fbq('init', '446676759119012');\n" +
                    "  fbq('track', 'PageView');" +
                    "</script>";

              $('head').append(fbpxlScript);

            }
        }
    };
})(jQuery);

/*
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '446676759119012');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=446676759119012&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
 */