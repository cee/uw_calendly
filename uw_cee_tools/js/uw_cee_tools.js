/**
 * @file
 */

(function ($) {

  Drupal.behaviors.uw_cee_tools = {
    attach: function (context, settings) {
      // calendly.
      if ($('ckcalendly').length) {
        $('ckcalendly').each(function () {
          calendlyurl = '/' + encodeURIComponent($(this).data('bdprofile'));
          //TODO update the dimensions
          $(this).before('<div class="calendly-inline-widget" data-url="https://calendly.com/'+ calendlyurl + '" style="width:100%;height:1400px; overflow-y: hidden;"></div>');
        });
      }
    }
  };
})(jQuery);