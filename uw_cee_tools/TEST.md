### Testing uw_cee_tools

The tests should be listed in the order they should be performed.

*Tests taken from ckeditor_socialmedia and modified.*

#### General Widget Testing
TEST: All buttons are present
> Go to node/add/uw-web-page; in body field's editor, confirm that there are buttons present for each of the social media types provided

TEST: Button titles appear as expected
> For the <a> tag for each button, the title should read "Add/Edit" followed by a description of the button's output (e.g. "Facebook Widget", "Tableau Visualization"); for consistency with the rest of the editor, these titles should be in title case (e.g. "Add/Edit Facebook Widget" not "Add/edit Facebook widget").

TEST: Clicking button opens the correct dialog box
> When you click the button, the dialog box for that widget type should appear.

TEST: Dialog box behaves as expected
> Each dialog box should contain the necessary elements to create that widget. Any interactive elements should adjust the dialog box as necessary (e.g. selecting from the "Twitter widget type" dropdown of the Twitter widget should modify the help text, and possibly the fields).

TEST: Cancelling creating a widget behaves as expected, when creating a widget
> When in the process of creating a widget, pressing cancel, hitting ESC, or closing the dialog box via the "X" should simply close the dialog box, without performing any entry validation and without creating a widget.

TEST: Validation behaves as expected when creating a widget
> When clicking "OK" or hitting ENTER, validation should take place. All required elements are expected to have some form of validation. If we know the parameters of a field (for example, in a URL field the entry should match the appearance of a valid URL), then the validation should trigger errors for information that fails to match the parameters. We must test for all known possible variants of the widget. Validation should only appear once per submission attempt, but must appear at every submission attempt if applicable. Validation messages should show all applicable errors, if possible in the order they occur in the form. Fixing some but not all validation errors, then submitting, should show only the currently applicable errors. When validation errors show, the dialog box should remain open with the entered information in place, and no widget should be created.

TEST: Creating a widget behaves as expected within the editor
> Once data entry passes validation, it should create a placeholder image for the widget with the appropriate icon and title text. The title text should describe the specific implementation (e.g. "Twitter widget: Tweets by @uwaterloo_it"). When viewing source, the element for the widget should contain the entered information in the expected attributes.

TEST: Double-clicking an existing widget opens the correct dialog box
> When double-clicking an existing widget, the editor should open the correct dialog box.
NOTE: This currently fails; we show a notice instead.

TEST: Selecting an existing widget, then clicking on the toolbar icon for that widget type, opens the correct dialog box.
> When single-clicking an existing widget, then clicking on the toolbar icon for that widget type, the editor should open the correct dialog box.

TEST: Right-clicking an existing widget shows a menu item to edit the correct widget type.
> When right-clicking an existing widget, the last item in the pop-up menu should be "Edit" followed by a description of the button's output (matching the text from the button titles). The icon for this menu item should be the correct icon for the widget type.

TEST: Selecting the edit option from an existing widget's right click menu opens the correct dialog box.
> When selecting the edit option from an existing widget's right click menu, the editor should open the correct dialog box.

TEST: When editing an existing widget, the dialog box contains the correct information.
> When editing an existing widget through any of the methods noted above, the information that was originally entered when the widget was created should appear in the appropriate fields. No other fields should have information. If the widget has multiple display options (e.g. the Twitter widget), the option for the selected widget should be chosen.

TEST: When cancelling editing an existing widget, the widget remains as it was before editing began
> When in the process of creating a widget, pressing cancel, hitting ESC, or closing the dialog box via the "X" should simply close the dialog box, without performing any entry validation and without making any changes to the existing widget, even if changes were made in the dialog box.

TEST: Validation behaves as expected when editing an existing widget
> All tests from "Validation behaves as expected when creating a widget" should be confirmed to work on existing widgets.

TEST: Submitting an edited widget behaves as expected within the editor
> Once data entry passes validation, it should update the existing placeholder image. Submitting should not create any additional widgets. The title text should describe the current specific implementation (e.g. "Twitter widget: Tweets by @uwaterloo)". When viewing source, the element for the widget should contain the entered information in the expected attributes.

TEST: Widget placeholders are responsive within the editor
> Widgets should resize as appropriate to fit within their container; for example, when placed in columns

TEST: "Real" widgets appear on the node page
> When the node is saved, widgets should show the intended "real" widget.
NOTE: Some widgets will fail if invalid information is provided; this is acceptable as long as valid information works and the invalid information matches the expected pattern. All widgets must be tested with valid information.

TEST: "Real" widgets are responsive on the node page
> Widgets should resize as appropriate to fit within their container; for example, when placed in columns

TEST: "Real" widgets should work or have some sort of fallback when Javascript is disabled
> With Javascript disabled, there should be some sort of fallback if the widget is not functional when Javascript is disabled. This usually is a link back to the original content. (It is then up to the content provider to make that page work without Javascript.) There should be no additional white space from where the widget would normally appear.

####Calendly Exclusive testing
Test: Right person appears
>Ensure that the right person's calendly appears on the web-page

Test: Can schedule a meeting
>Book a meeting and see if meeting goes through. Person booking the meeting should recieve an email that they provided.
