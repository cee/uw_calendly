![calendly](../images/calendly_banner.jpg)
## UW CEE Tools Module
#### Current supported widgets:
*Calendly*
#### Use:
This module replaces custom tags with JS embeds from sites. Currently there exists a non-JS function but is not in use.
#### Setup:
1. Enable plug-in at admin/config/content/wysiwyg/profile by selecting a profile and enabling the desired plugins
2. Ensures that there is CSP exception for https://calendly.com & https://*.calendly.com at /etc/apache2/conf-available/wcms-common.conf in your local server
3. Edit filters to enable ckcalendly tags.

#### Dialog Box Options:
##### Calendly:
>Calendly link: https//calendly.com/

>This is your page URL minus the domain name and protocol (eg. https://calendly.com/).

*Credits to Kimly Truong for icons*

*Banner might change as tools are added*