/**
 * @file
 * ckeditor plug-in for CEE
 */
var doubleclick_element;

CKEDITOR.plugins.add('uw_cee_tools', {
  requires : ['dialog', 'fakeobjects'],
  init: function (editor){
    //Define plugin names.
    var pluginName = 'uw_cee_tools';

    //Set up objects to share variables amongst scripts.
    CKEDITOR.uw_cee_tools = {};

    //Register button for Calendly
    editor.ui.addButton('Calendly', {
      label : "Add/Edit Calendly Calendar",
      command : 'calendly',
      icon : this.path + 'icons/calendly.png'
    });

    //Register right-click menu item for Calendly
    editor.addMenuItems({
      calendly : {
        label: "Edit Calendly Calendar",
        icon : this.path + 'icons/calendly.png',
        command : 'calendly',
        group : 'image',
        order : 1
      }
    });
    //Make ckcalendly a self-closing tag.
    CKEDITOR.dtd.$empty['ckcalendly'] = 1;
    //Making sure that the fake element for calendly has a name.
    CKEDITOR.uw_cee_tools.ckcalendly = 'Calendly widget';
    CKEDITOR.lang.en.fakeobjects.ckcalendy = CKEDITOR.uw_cee_tools.ckcalendly;
    //Add JS file that defines the dialog box for calendly.
    CKEDITOR.dialog.add('calendly',this.path + 'dialogs/calendly.js');
    editor.addCommand('calendly', new CKEDITOR.dialogCommand('calendly'));
    //Regex
    CKEDITOR.uw_cee_tools.calendly_regex = /^[a-zA-Z0-9_-]+$/;

    //Open the appropriate dialog box if an element is double-clicked.
    editor.on('doubleclick', function (evt) {
      var element = evt.data.element;

      //Store in global variable.
      doubleclick_element = element;

      if (element.is('img') && element.data('ck-real-element-type') === 'ckcalendly') {
        evt.data.dialog = 'calendly';
      }
    });

    //Adding the appropriate right-click menu item if an element is right-clicked
    if (editor.contextMenu) {
      editor.contextMenu.addListener(function (element,selection) {
            if (element && element.is('img') && element.data('ck-real-element-type') === 'ckcalendly'){
              return {calendly : CKEDITOR.TRISTATE_OFF};
            }
      });
    }

    //Add CSS to use in-editor to style custom fake elements.
    CKEDITOR.addCss(
      'img.ckcalendly {' +
      'background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/big/calendly.png') + ');' +
      'background-position: center center;' +
      'background-repeat: no-repeat;' +
      'background-color: #000;' +
      'width: 100%;' +
      'height: 350px;' +
      'margin: 0 0 10px 0;' +
      'margin-left: auto;' +
      'margin-right: auto;' +
      '}'+

      // calendly: Definitions for wide width.
      '.uw_tf_standard_wide img.ckcalendly {' +
      'max-height: 1050px;' +
      '}' +
      '.uw_tf_standard_wide .col-50 img.ckcalendly {' +
      'max-height: 504px;' +
      '}' +
      '.uw_tf_standard_wide .col-33 img.ckcalendly {' +
      'max-height: 336px;' +
      '}' +
      '.uw_tf_standard_wide .col-66 img.ckcalendly {' +
      'max-height: 672px;' +
      '}' +
      '.uw_tf_standard_wide .threecol-33 img.ckcalendly {' +
      'max-height: 326px;' +
      '}' +

      // calendly: Definitions for standard width.
      '.uw_tf_standard img.ckcalendly {' +
      'max-height: 700px;' +
      '}' +
      '.uw_tf_standard .col-50 img.ckcalendly {' +
      'max-height: 336px;' +
      '}' +
      '.uw_tf_standard .col-33 img.ckcalendly {' +
      'max-height: 228px;' +
      '}' +
      '.uw_tf_standard .col-66 img.ckcalendly {' +
      'max-height: 448px;' +
      '}' +
      '.uw_tf_standard .threecol-33 img.ckcalendly {' +
      'max-height: 217px;' +
      '}'   
    );
  },
  afterInit : function (editor) {
    //make fake image display on first load/return from source view
    if (editor.dataProcessor.dataFilter){
      editor.dataProcessor.dataFilter.addRules({
        elements: {
          ckcalendly : function (element) {
            //Reset tittle
            CKEDITOR.lang.en.fakeobjects.ckcalendly = CKEDITOR.uw_cee_tools.ckcalendly;
            //Adjust title if a url is present
            if (element.attributes['data-bdprofile']){
              CKEDITOR.lang.en.fakeobjects.ckcalendly += ': ';
              CKEDITOR.lang.en.fakeobjects.ckcalendly += element.attributes['data-bdprofile'];
            }
            //Note that this just accepts whatever attributes are on the element; may want to filter these.
            return editor.createFakeParserElement(element, 'ckcalendly','ckcalendly', false);
          }
        }
      });
    }
  }
});
