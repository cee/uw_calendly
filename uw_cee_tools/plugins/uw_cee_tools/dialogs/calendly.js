/**
 * @file
 */

(function () {
  // Variable used to track when the form is submitted.
  // i.e to check for double submissions on using enter key in dialog.
  var formSubmitted = false;
  var calendlyDialog = function (editor) {
    return {
      title : 'Calendly Calendar',
      minWidth : 625,
      minHeight : 100,
      contents: [{
        id: 'calendly',
        label: 'calendly',
        elements:[{
          type: 'text',
          id: 'calendlyInput',
          label: '<strong>Calendly link:</strong> https://calendly.com/&lt;user&gt;(required)',
          setup: function (element) {
            this.setValue(element.getAttribute('data-bdprofile'));
          }
        },]
      }],
      onOk: function () {
        // Get form information.
        calendlyInput = this.getValueOf('calendly','calendlyInput');
        // Validate input. Note that there is probably a CKEditor specific way to do this, but this works.
        errors = '';
        if (!CKEDITOR.uw_cee_tools.calendly_regex.test(calendlyInput)) {
          errors += "You must enter a valid Calendly ID.\r\n";
        }

        // If form has been submitted before then set it back to not being seeing before.
        // i.e if this is double submission set it back to not being run before.
        if (formSubmitted == true) {
          formSubmitted = false;
          return false;
        }
        // Only display errors if there are errors to display and the form has not been run before.
        else if (errors && formSubmitted == false) {
          alert(errors);
          formSubmitted = true;
          return false;
        }
        else {
          // Create the ckcalendly element.
          var ckcalendlyNode = new CKEDITOR.dom.element('ckcalendly');
          // Save contents of dialog as attributes of the element.
          ckcalendlyNode.setAttribute('data-bdprofile',calendlyInput);
          // Adjust title based on user input.
          CKEDITOR.lang.en.fakeobjects.ckcalendly = CKEDITOR.uw_cee_tools.ckcalendly + ': ' + calendlyInput;
          // Create the fake image for this element and insert into the document (realElement, className, realElementType, isResizable).
          var newFakeImage = editor.createFakeElement(ckcalendlyNode, 'ckcalendly', 'ckcalendly', false);
          newFakeImage.addClass('ckcalendly');
          if (this.fakeImage) {
            newFakeImage.replace(this.fakeImage);
            editor.getSelection().selectElement(newFakeImage);
          }
          else {
            editor.insertElement(newFakeImage);
          }
          // Reset title.
          CKEDITOR.lang.en.fakeobjects.ckcalendly = CKEDITOR.uw_cee_tools.ckcalendly;
        }
      },
      onShow: function () {
        // Set up to handle existing items.
        this.fakeImage = this.ckcalendlyNode = null;
        var fakeImage;

        // Check if element is right clicked or icon was clicked, if not use global varaible doubleclick_element.
        if (this.getSelectedElement()) {
          fakeImage = this.getSelectedElement();
        }
        else {
          fakeImage = doubleclick_element;
        }

        if (fakeImage && fakeImage.data('cke-real-element-type') && fakeImage.data('cke-real-element-type') === 'ckcalendly') {
          this.fakeImage = fakeImage;
          var ckcalendlyNode = editor.restoreRealElement(fakeImage);
          this.ckcalendlyNode = ckcalendlyNode;
          this.setupContent(ckcalendlyNode);
        }
      }
    }
  };

  CKEDITOR.dialog.add('calendly', function (editor) {
    return calendlyDialog(editor);
  });
})();
