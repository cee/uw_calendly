# API Integration for ckeditor

### Folders in this project:
#### uw_cee_calendly:
![calendly](images/calendly_banner.jpg)
>Current module that contains CKeditor plugin to add widgets for calendly. See README file in folder for more info.
#### pxl_trkr:
![pxl_trkr](images/pixel_tracker_banner.jpg)
>Module that enables tracking pixels on only certain pages.
#### Api_key:
>Contains the api key for emily-burgess *not currently in use*

#### calendly(deprecated):
>Old module containing old implementation of calendly api

*Credits to Kimly Truong for icons*